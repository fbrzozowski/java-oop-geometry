package com.codecool.java.geometry.containers;

import com.codecool.java.geometry.shapes.*;
import java.lang.reflect.*;

import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;

public class ShapeList {

    protected List<Shape> shapes;

    public ShapeList() {
        this.shapes = new ArrayList<Shape>();
    }

    public void addShape(Shape shape) {
        this.shapes.add(shape);
    }

    public Shape getShapeAt(int index) {
        return this.shapes.get(index);
    }

    public String getShapesTable() {
        StringBuilder table = new StringBuilder();
        for(int i=0;i<this.shapes.size();i++) {
            
            Shape shape = this.shapes.get(i);
            String className = shape.getClass().getName();
            String areaFormula = invokeStaticMethod("getAreaFormula", className); 
            String perimeterFormula = invokeStaticMethod("getPerimeterFormula", className); 

            table.append(i)
                .append(" | ")
                .append(shape.getClass().getSimpleName())
                .append(" | ")
                .append(shape.calculatePerimeter())
                .append(" | ")
                .append(perimeterFormula)
                .append(" | ")
                .append(shape.calculateArea())
                .append(areaFormula)
                .append(" | \n");
        }
        return table.toString();
    }

    public static String invokeStaticMethod(String methodName, String className) {
        try {
                Class c = Class.forName(className);
                Method m = c.getDeclaredMethod(methodName);
                Object o = m.invoke(null);
                return o + "";
            } catch (Exception e) {
                return "null";
            }
    }

    public static void getLargestShapeByPerimeter() {
        //
    }

    public static void getLargestShapeByArea() {
        //
    }
}
